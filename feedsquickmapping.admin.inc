<?php

/**
 * @file
 * Administrative functions.
 */

/**
 * Whether or not Feeds is language aware.
 *
 * Set this to TRUE if you applied a patch that adds support for language.
 * @see http://drupal.org/node/1183440
 *
 * @var bool
 */
define('FEEDSQUICKMAPPING_LANGUAGE_AWARE', FALSE);

/**
 * Position in comma-separated mapping list for 'source' column.
 *
 * @var int
 */
define('FEEDSQUICKMAPPING_SOURCE_COLUMN', 0);

/**
 * Position in comma-separated mapping list for 'target' column.
 *
 * @var int
 */
define('FEEDSQUICKMAPPING_TARGET_COLUMN', 1);

/**
 * Position in comma-separated mapping list for 'unique' column.
 *
 * @var int
 */
define('FEEDSQUICKMAPPING_UNIQUE_COLUMN', 2);

/**
 * Position in comma-separated mapping list for 'language' column.
 *
 * @var int
 */
define('FEEDSQUICKMAPPING_LANGUAGE_COLUMN', 3);

/**
 * Position in comma-separated mapping list for 'target configuration' column.
 *
 * @var int
 */
define('FEEDSQUICKMAPPING_TARGET_CONFIG_COLUMN', 4);

/**
 * Form callback for mapping.
 *
 * @see feedsquickmapping_preprocess_feeds_ui_edit_page()
 */
function feedsquickmapping_mapping_form($form, &$form_state, $importer) {
  $form = array();
  $form['#importer'] = $importer->id;
  $form['#mappings'] = $mappings = $importer->processor->getMappings();
  $form['help']['#markup'] = feeds_ui_mapping_help();
  $form['#prefix'] = '<div id="feeds-ui-mapping-form-wrapper">';
  $form['#suffix'] = '</div>';

  // Get mapping sources from parsers and targets from processor, format them
  // for output.
  // Some parsers do not define mapping sources but let them define on the fly.
  $sources = $importer->parser->getMappingSources();
  if ($sources) {
    $source_options = _feeds_ui_format_options($sources);
    foreach ($sources as $k => $source) {
      $legend['sources'][$k]['name']['#markup'] = empty($source['name']) ? $k : $source['name'];
      $legend['sources'][$k]['description']['#markup'] = empty($source['description']) ? '' : $source['description'];
    }
  }
  else {
    $legend['sources']['#markup'] = t('This parser supports free source definitions. Enter the name of the source field in lower case into the Source text field above.');
  }
  $targets = $importer->processor->getMappingTargets();
  $legend['targets'] = array();
  foreach ($targets as $k => $target) {
    $legend['targets'][$k]['name']['#markup'] = empty($target['name']) ? $k : $target['name'];
    $legend['targets'][$k]['description']['#markup'] = empty($target['description']) ? '' : $target['description'];
  }

  // Legend explaining source and target elements.
  $form['legendset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Legend'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree' => TRUE,
  );
  $form['legendset']['legend'] = $legend;

  // Transform mappings in comma separated text.
  $mappings_text = '';
  foreach ($form['#mappings'] as $mapping) {
    $mapping_csv = array(
      FEEDSQUICKMAPPING_SOURCE_COLUMN => NULL,
      FEEDSQUICKMAPPING_TARGET_COLUMN => NULL,
      FEEDSQUICKMAPPING_UNIQUE_COLUMN => NULL,
    );
    foreach ($mapping as $property => $value) {
      switch ($property) {
        case 'source':
          $mapping_csv[FEEDSQUICKMAPPING_SOURCE_COLUMN] = trim($mapping[$property]);
          break;
        case 'target':
          $mapping_csv[FEEDSQUICKMAPPING_TARGET_COLUMN] = trim($mapping[$property]);
          break;
        case 'unique':
          $mapping_csv[FEEDSQUICKMAPPING_UNIQUE_COLUMN] = (int) $mapping['unique'];
          break;
        case 'language':
          $mapping_csv[FEEDSQUICKMAPPING_LANGUAGE_COLUMN] = trim($mapping[$property]);
          break;
        default:
          // Other properties are assumed to be target configuration.
          $mapping_csv[FEEDSQUICKMAPPING_TARGET_CONFIG_COLUMN][] = $property . ':' . $mapping[$property];
          break;
      }
    }
    // Target configuration mapping can be multiple columns.
    if (isset($mapping_csv[FEEDSQUICKMAPPING_TARGET_CONFIG_COLUMN])) {
      $mapping_csv[FEEDSQUICKMAPPING_TARGET_CONFIG_COLUMN] = implode(',', $mapping_csv[FEEDSQUICKMAPPING_TARGET_CONFIG_COLUMN]);
    }
    // Write CSV line.
    $mappings_text .= implode(',', $mapping_csv) . "\n";
  }

  $columns = array(
    t('source'),
    t('target'),
    t('unique'),
  );
  if (FEEDSQUICKMAPPING_LANGUAGE_AWARE) {
    $columns[] = t('language');
  }
  $columns[] = t('target configuration');

  // Textarea to edit the mappings.
  $form['mappings'] = array(
    '#type' => 'textarea',
    '#title' => t('Mapping (!columns)', array('!columns' => implode(', ', $columns))),
    '#description' => t('Enter the mappings comma separated.'),
    '#default_value' => $mappings_text,
  );

  // Save button.
  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit handler for save button on feeds_ui_mapping_form().
 */
function feedsquickmapping_mapping_form_submit($form, &$form_state) {
  $importer = feeds_importer($form['#importer']);

  // Read mappings.
  $mappings = array();
  $mappings_text = $form_state['values']['mappings'];
  $rows = explode("\n", $mappings_text);
  foreach ($rows as $row) {
    $mappingrow = explode(',', $row);
    if (count($mappingrow) > 1) {
      $mapping = array(
        'source' => NULL,
        'target' => NULL,
        'unique' => 0,
      );
      for ($i = 0; $i < count($mappingrow); $i++) {
        switch ($i) {
          case FEEDSQUICKMAPPING_SOURCE_COLUMN:
            $mapping['source'] = trim($mappingrow[$i]);
            break;
          case FEEDSQUICKMAPPING_TARGET_COLUMN:
            $mapping['target'] = trim($mappingrow[$i]);
            break;
          case FEEDSQUICKMAPPING_UNIQUE_COLUMN:
            $mapping['unique'] = trim($mappingrow[$i]);
            break;
          default:
            if (FEEDSQUICKMAPPING_LANGUAGE_AWARE && $i == FEEDSQUICKMAPPING_LANGUAGE_COLUMN) {
              $mapping['language'] = !empty($mappingrow[$i]) ? trim($mappingrow[$i]) : 'und';
            }
            else {
              list($column, $value) = explode(':', $mappingrow[$i]);
              $mapping[$column] = trim($value);
            }
            break;
        }
      }
      $mappings[] = $mapping;
    }
  }

  try {
    $importer->processor->addConfig(array('mappings' => $mappings));
    $importer->processor->save();
    drupal_set_message(t('Mapping has been saved.'));
  }
  catch (Exception $e) {
    drupal_set_message($e->getMessage(), 'error');
  }
}

/**
 * Theme function for feedsquickmapping_mapping_form().
 */
function theme_feedsquickmapping_mapping_form(&$variables) {
  $form = $variables['form'];
  $output = '<div class="help feeds-admin-ui""' . drupal_render($form['help']) . '</div>';
  $output .= drupal_render($form['mappings']);

  // Build the help table that explains available sources.
  $legend = '';
  $rows = array();
  foreach (element_children($form['legendset']['legend']['sources']) as $k) {
    $rows[] = array(
      check_plain($k),
      check_plain(drupal_render($form['legendset']['legend']['sources'][$k]['description'])),
    );
  }
  if (count($rows)) {
    $legend .= '<h4>' . t('Sources') . '</h4>';
    $legend .= theme('table', array('header' => array(t('Name'), t('Description')), 'rows' => $rows));
  }

  // Build the help table that explains available targets.
  $rows = array();
  foreach (element_children($form['legendset']['legend']['targets']) as $k) {
    $rows[] = array(
      check_plain($k),
      check_plain(drupal_render($form['legendset']['legend']['targets'][$k]['description'])),
    );
  }
  $legend .= '<h4>' . t('Targets') . '</h4>';
  $legend .= theme('table', array('header' => array(t('Name'), t('Description')), 'rows' => $rows));

  // Stick tables into collapsible fieldset.
  $form['legendset']['legend'] = array(
    '#markup' => '<div>' . $legend . '</div>',
  );

  $output .= drupal_render($form['legendset']);

  $output .= drupal_render_children($form);
  return $output;
}
