Feeds Quick Mapping
-------------------------
This a simple module that allows you to add field mapping comma separated. This
can be handy when you want to make a lot of modifications to the mapping at once
and don't want to wait for each form submit.

This module requires Feeds 7.x-2.0-alpha7 or higher.
